## Agave Apache SSL Proxy

This is the proxy server used to expose Agave's backend APIs to the outside world. It contains an Apache server with SSL enabled and rewrite rules to each of Agave's container APIs. Containers are linked at runtime, based on hostname, so load balancing can be inserted in front of an API by deploying the load balancer container and exposing it at the service's expected hostname (ie. apps).

## What is the Agave Platform?

The Agave Platform ([http://agaveapi.co](http://agaveapi.co)) is an open source, science-as-a-service API platform for powering your digital lab. Agave allows you to bring together your public, private, and shared high performance computing (HPC), high throughput computing (HTC), Cloud, and Big Data resources under a single, web-friendly REST API.

* Run scientific codes

  *your own or community provided codes*

* ...on HPC, HTC, or cloud resources

  *your own, shared, or commercial systems*

* ...and manage your data

  *reliable, multi-protocol, async data movement*

* ...from the web

  *webhooks, rest, json, cors, OAuth2*

* ...and remember how you did it

  *deep provenance, history, and reproducibility built in*

For more information, visit the [Agave Developer’s Portal](http://agaveapi.co) at [http://agaveapi.co](http://agaveapi.co).


## Using this image

This image will set up an Apache server that proxies connections to the backend Agave APIs. The connections to the Java APIs happen over AJP. The connections to the PHP and Python APIs happen vis standard proxy. Both SSL and standard communication proxy identically. 

### Running this image

This image extends the trusted php:5.4-apache image.

    > docker run -d -h docker.example.com         	\
               -p 80:80                             \ # Apache
               -p 443:443                           \ # Apache + TLS
               --name apache-proxy
               agaveapi/apache-ssl-proxy

### SSL Certs

This image will create a new set of SSL certs every time it runs. To avoid this, you can mount the `/etc/httpd/ssl` folder to the host machine and the certs will persist between container executions.

  > docker run -d -h docker.example.com
             -p 80:80                             \ # Apache
             -p 443:443                           \ # Apache + TLS
             -v /path/to/ssl/certs:/etc/httpd/ssl \ # SSL certs locally will be used.
             agaveapi/apache-ssl-proxy

If you are running Agave in production, you should use a set of trusted SSL certs issued from a reputable CA for your domain. Place these files in your mounted folder and the container will use those in leu of the generated certs. The expected names of the files are:

* server.crt: Passwordless PEM encoded certificate.
* server.key: Passwordless PEM encoded key.
* ca-bundle.crt: trusted certificates for client authentication or alternatively one huge file containing all of them (file must be PEM encoded).
* ca-chain.crt: concatenation of PEM encoded CA certificates which form the certificate chain for the server certificate. Alternatively the referenced file can be the same as SSLCertificateFile when the CA certificates are directly appended to the server certificate for convinience.

  > docker run -d -h your.domain.com          	 \
             -p 80:80                             \ # Apache
             -p 443:443                           \ # Apache + TLS
             -v /path/to/ssl/certs:/etc/httpd/ssl \ # SSL certs locally will be used.
             agaveapi/apache-ssl-proxy

### Logging

To access the server logs, mount a local folder to `/var/www/http` in the container. This will give you access to the Apache logs.

  > docker run -d -h docker.example.com         	\
             -p 80:80                             \ # Apache
             -p 443:443                           \ # Apache + TLS
             -v /path/to/ssl/certs:/etc/httpd/ssl \ # SSL certs locally will be used.
             -v `pwd`/logs:/var/www/http          \ # mount the log directory
             --name apache-proxy
             agaveapi/apache-ssl-proxy
